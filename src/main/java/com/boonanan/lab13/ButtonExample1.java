package com.boonanan.lab13;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ButtonExample1 extends JFrame {
    JButton button;
    JButton clearButton;
    JTextField text;

    public ButtonExample1() {
        super("Button Example");
        text = new JTextField();
        text.setBounds(50, 50, 150, 20);
        button = new JButton("Click");
        button.setBounds(50, 100, 95, 30);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Click Button");
                text.setText("Welcome to Burapha");
            }
        });
        clearButton = new JButton();
        clearButton.setBounds(165, 100, 95, 30);
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                text.setText("");
            }
        });
        this.setLayout(null);
        this.add(text);
        this.add(button);
        this.add(clearButton);
        this.setSize(400, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        ButtonExample1 frame = new ButtonExample1();
    }
}
