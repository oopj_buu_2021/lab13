package com.boonanan.lab13;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.*;

public class QueueApp extends JFrame {
    JTextField txtname;
    JLabel queuelist, current;
    JButton btnAdd, btnGet, btnClear;
    LinkedList<String> user;

    public QueueApp() {
        super("Queue App");
        user = new LinkedList<>();
        txtname = new JTextField();
        txtname.setBounds(30, 10, 200, 20);

        btnAdd = new JButton("Add Queue");
        btnAdd.setBounds(250, 10, 100, 20);

        btnGet = new JButton("Get Queue");
        btnGet.setBounds(250, 40, 100, 20);

        btnClear = new JButton("Clear Queue");
        btnClear.setBounds(250, 70, 100, 20);

        queuelist = new JLabel("Emty");
        queuelist.setBounds(30, 40, 200, 20);

        current = new JLabel("?");
        current.setHorizontalAlignment(JLabel.CENTER);
        current.setFont(new Font("serif", Font.PLAIN, 50));
        current.setBounds(30, 70, 200, 50);

        txtname.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();

            }
        });

        btnClear.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                clearQueue();

            }
        });

        btnGet.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                getQueue();

            }
        });

        btnAdd.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();

            }
        });

        ShowQueue();
        this.add(current);
        this.add(queuelist);
        this.add(btnClear);
        this.add(btnGet);
        this.add(btnAdd);
        this.add(txtname);
        this.setLayout(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(400, 300);

    }

    public void ShowQueue() {
        if (user.isEmpty()) {
            queuelist.setText("Emty");
        } else {
            queuelist.setText(user.toString());
        }

    }

    public void addQueue() {
        String name = txtname.getText();
        if (name.equals("")) {
            return;
        }
        user.add(name);
        txtname.setText("");
        ShowQueue();
    }

    public void getQueue() {
        if (user.isEmpty()) {
            current.setText("?");
            return;
        }
        String name = user.remove();
        current.setText(name);
        ShowQueue();
    }

    public void clearQueue() {
        user.clear();
        current.setText("?");
        txtname.setText("");
        ShowQueue();
    }

    public static void main(String[] args) {
        QueueApp queue = new QueueApp();
    }
}
