package com.boonanan.lab13;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class HelloMyName extends JFrame {
    JLabel jlname ;
    JTextField txtname;
    JButton btnHello;
    JLabel jhello;
    public HelloMyName(){
        super("Hello My Name");
        jlname = new JLabel("Name :");
        txtname = new JTextField();
        btnHello = new JButton("Hello");
        jhello = new JLabel("Hello My Name");
        btnHello.setBounds(10,40,250,20);
        jlname.setBounds(10,10,100,20);
        txtname.setBounds(60,10,200,20);
        jhello.setBounds(30,70,250,20);
        jhello.setHorizontalAlignment(JLabel.CENTER);
        jlname.setHorizontalAlignment(JLabel.RIGHT);

        btnHello.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                String myname = txtname.getText();
                jhello.setText("Hello " + myname);
                
            }});


        this.add(btnHello);
        this.add(txtname);
        this.add(jlname);
        this.add(jhello);

        this.setSize(400,300);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
     public static void main(String[] args) {
        HelloMyName name = new HelloMyName();
     }
}
